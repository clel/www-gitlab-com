---
layout: handbook-page-toc
title: "People Compliance at GitLab"
description: "People Compliance at GitLab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## People Compliance Mission

People Compliance works collaboratively with multiple functional teams throughout the GitLab organization. We partner to manage and protect the privacy of our People's (team members) personal information, all relevant documents through data retention policies, and understanding all global compliance requirements to help meet internal and stakeholder expectations. Our primary partners are the entire People Group, Legal team, and Business Operations team. 

## Contact the Compliance Team

* Email
   * `people-compliance@gitlab.com`
* Tag us in GitLab
   * `@gl-peoplecompliance`
* Slack
   * The #peopleops Slack channel is the best place for questions relating to our team.

## People Compliance Core Competencies & Responsibilities

All Responsibilities are listed in the [People Compliance job family page](https://about.gitlab.com/job-families/people-ops/people-compliance/), and they can be summarized as follows:

1. To identify needs and propose strategies to collaborate and support the enablement of compliant protocols.
1. Implement compliance initiatives and templates that can be utilised for initiatives including: document retention policy for team member information, data protection and annual training on these. 
1. Support team members, managers and teams to ensure they are aligned with our policies.
1. Iterate to simplify and ensure processes are efficient and automated as much as possible.  
1. Ensure that all People Group and GitLab team member's personal information is secure and can pass key audits. 

## How we work

Our [People Compliance Issue Board](https://gitlab.com/gitlab-com/people-group/compliance/-/boards/2082154) serves as the single source of truth on the People Compliance team's priorities. If you have a request for the People Compliance team, please [open a Compliance Request issue](https://gitlab.com/gitlab-com/people-group/compliance/-/blob/master/.gitlab/issue_templates/compliance_request.md) and we will review and prioritize.

### Labels

Please see the [README.md](https://gitlab.com/gitlab-com/people-group/compliance/-/blob/master/README.md) in the People Compliance project listing our current status and communication labels. The main workflow stages are as follows:

**Stages**

- `workflow::triage`: New issue and no triaging has been done on the issue.
- `workflow::research`: Issue currently being scoped/considered but are not being actively worked on yet.
- `workflow::in progress`: We are working on this now, towards our due date.
- `workflow::blocked`: We worked on the issue and waiting for a response or another issue or MR to unblock us.
- `workflow::backlog`: Adding to the backlog of work todo and add a due date.
- `workflow::done`

## Relevant Handbook Pages

* [Privacy](/handbook/legal/privacy/) and [Privacy Laws and GitLab](/handbook/legal/privacy/privacy-laws.html) - More information on GitLab's privacy practices.
* [Employee Privacy Policy](/handbook/legal/employee-privacy-policy/) - GitLab's policy for how we handle the personal information of our team members.
* [Records Retention Policy](/handbook/legal/record-retention-policy/) - GitLab's policy on the implementation of procedures, best practices, and tools to promote consistent life cycle management of GitLab records.
* [DM.7.03 - Data Retention and Disposal Policy ](/handbook/engineering/security/security-assurance/security-compliance/guidance/DM.7.03_data_retention_and_disposal_policy.html) - GitLab's Security Compliance policies related to data retention and disposal.
* [Data Protection Impact Assessment (DPIA) Policy](/handbook/engineering/security/dpia-policy/) - GitLab's policies related to Data Protection.
* [GitLab Audit Committee](/handbook/board-meetings/committees/audit/) - Information regarding GitLab's Audit Committee of the Board of Directors of GitLab Inc. 
* [Total Rewards Audits](/handbook/total-rewards/total-rewards-audits/) - Information on some of our People Group audits.


## Performance Indicators

#### [Compliance, data retention and implementation](https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#compliance-data-retention-and-implementation)

#### [Google Drive documentation](https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#compliance-data-retention-and-implementation)

#### [Implementation of audits across Team Member Experience tasks](https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#compliance-data-retention-and-implementation)

<details markdown="1">

<summary>Meet the People Compliance team</summary>

## Meet the People Compliance team

[**Cassiana Gudgenov**](https://about.gitlab.com/company/team/#cgudgenov)

* Title: People Compliance Specialist
* GitLab handle: @cgudgenov
* Slack handle: @cassiana gudgenov

</details>
