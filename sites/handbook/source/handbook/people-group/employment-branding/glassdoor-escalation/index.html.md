---
layout: handbook-page-toc
title: "Process for Tracking and Escalating Glassdoor Reviews"
---

# Why we track and respond to company reviews

Glassdoor reviews and ratings have valuable themes and trends that can help us improve our culture, hiring process, benefits, and more. 
We share ratings trends, key themes, or escalations to the appropriate teams internally so that we can track our success and take addition action if needed. This includes:
- People Group Key meeting monthly
- Direct escalations of negative reviews - require immediate action or response
- Recruiting team meeting - sharing hiring-related themes that have come up

Note: This page details the process for *Company* reviews on Glassdoor. The Candidate Experience team monitors and tracks Interview reviews on the platform. 

# Escalation process

The Talent Brand Manager monitors reviews on a weekly basis, and responds directly to ~50% of positive reviews. When the Talent Brand Manager sees a negative review posted, here is the process to ensure the review is escalated and responded to as soon as possible: 

1. Open an issue in this epic (WIP).
1. Determine the team or department the review is talking about, the status of the poster (current team member or former team member), and where the person is located. 
1. Based on the department, tag the appropriate People Business Partner in the issue for review. 
1. Determine the right escalation path within leadership of that specific department so that the leaders can be tagged and made aware.
1. Draft a response in a Google Doc (link in the issue) to the review in partnership with the People Business Partner and other DRIs within the impacted department. 
1. Get feedback and approval from the impacted department, and determine with the People Business Partner who will respond on behalf of the team. See [Who Should Respond](/#who-should-respond) section below.  

# Who Should Respond 

WIP 



