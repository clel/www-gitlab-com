---
layout: handbook-page-toc
title: "Core Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Becoming a Core Team member

A new member can be added to the [Core Team](/community/core-team/) at any time through the following steps:

1.  Current Core Team members can nominate a new member from the wider community at any time using a confidential issue in [the Core Team group](https://gitlab.com/groups/gitlab-core-team/-/issues).
2.  The nominee will be added to the Core Team if they accept the nomination and have received positive votes from two-thirds (2/3) of all current core team members within a two week period.
3.  Once a new member has been added, start the onboarding process by following the steps outlined in the [Core Team member orientation section](/handbook/marketing/community-relations/code-contributor-program/core-team/#core-team-member-orientation) below. 
    - Core Team members are required to sign an NDA as a part of the onboarding process. 
    - For Slack access, file an issue for multi-channel access following this [example](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/6071).

## Monthly Core Team meetings

The Core Team meets once a month and anyone is welcome to join the call. Call logistics/agenda/notes for the meeting are available on the [Core Team meeting page](https://gitlab.com/gitlab-core-team/general/wikis/monthly-core-team-meeting). All meeting recordings are available at the [Core Team meeting Playlist](https://www.youtube.com/playlist?list=PLFGfElNsQthZ12EUkq3N9QlThvkf3WGnZ). 

## Contacting Core Team members

Core Team members can be reached by [mentioning](https://docs.gitlab.com/ee/user/group/subgroups/index.html#mentioning-subgroups) `@gitlab-core-team` in issues or merge requests. 

While GitLab is the primary means of contact, the Core Team can also be contacted on the [#core](https://gitlab.slack.com/messages/core) Slack channel.

[Service Desk](https://gitlab.com/gitlab-core-team/general/issues/service_desk) will also be used as a communication tool for Core Team members. Anyone can open an issue in the `gitlab-core-team` project or contact the core team through email by sending an email directly to `incoming+gitlab-core-team/general@incoming.gitlab.com`.  Core Team members who signed NDAs will also have access to GitLab Slack channels.

## Offboarding and stepping down gracefully 

If you are no longer able to or interested in serving in the Core Team, you should make an annoucement on the `#core` Slack channel. When you step down, you will become a [Core Team Alumni](/community/core-team/alumni/). Once a Core Team member steps down, GitLab team member(s) will start the off-boarding activities to: 

1.  Move the individual from the `team.yml` file to the `alumni.yml` file.
2.  Create an issue in [team-member-epics/employment](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues) using the [`offboarding` template](https://gitlab.com/gitlab-com/people-group/employment-templates-2/-/blob/master/.gitlab/issue_templates/offboarding_core_team_member.md) and fill out the "Core Team Members" section to remove the individual from GitLab Slack, [the Core Team Group](https://gitlab.com/groups/gitlab-core-team/community-members/-/group_members), gitlab-org etc.

## Core Team Member Orientation

1.  Create an issue in the [Lifecycle Management Project](https://gitlab.com/gitlab-com/temporary-service-providers/lifecycle) using the [Core Team Member Orientation Issue Template](https://gitlab.com/gitlab-com/temporary-service-providers/lifecycle/-/issues/new?issuable_template=orientation_issue_core_member) and follow the steps outlined.
