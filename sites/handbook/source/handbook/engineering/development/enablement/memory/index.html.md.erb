---
layout: handbook-page-toc
title: Memory Team
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

Build and enhance a highly performant GitLab product portfolio, and establish best practices for performance oriented development.

## Mission

The Memory team is responsible for optimizing GitLab application performance by managing the memory resources required. The team is also responsible for changes affecting the responsiveness of the application.  We will accomplish these goals in two ways. First we will research and resolve reported memory issues to the best of our ability and within our published SLAs.  Additionally, will build tooling to proactively inform developers of the static and dynamic memory consumption of their proposed changes.  You can read more about our approach, in detail, [here](./approach.html).

## Team Members

The following people are permanent members of the Memory Team:

<%-# Hard coding the team makeup for now until team.yml reflects the team structure -%>
<%#= direct_team(manager_role: 'Backend Engineering Manager, Memory') %>
<%-# Hard coding the team makeup for now until team.yml reflects the team structure -%>
<table>
<thead>
<tr>
<th>Person</th>
<th>Role</th>
</tr>
</thead>
<tbody>
<tr>
<td>Craig Gomes</td>
<td><a href="/job-families/engineering/backend-engineer/#engineering-manager">Engineering Manager, Memory & Database</a></td>
</tr>
<tr>
<td>Kamil Trzciński</td>
<td><a href="/job-families/engineering/distinguished-engineer/">Distinguished Engineer, Ops and Enablement</a></td>
</tr>
<tr>
<td>Qingyu Zhao</td>
<td><a href="/job-families/engineering/backend-engineer">Senior Backend Engineer, Memory</a></td>
</tr>
<tr>
<td>Aleksei Lipniagov</td>
<td><a href="/job-families/engineering/backend-engineer">Senior Backend Engineer, Memory</a></td>
</tr>
<tr>
<td>Nikola Milojevic</td>
<td><a href="/job-families/engineering/backend-engineer">Senior Backend Engineer, Memory</a></td>
</tr>
<tr>
<td>Matthias Käppler</td>
<td><a href="/job-families/engineering/backend-engineer">Senior Backend Engineer, Memory</a></td>
</tr>
</tbody>
</table>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%#= stable_counterparts(role_regexp: /[,&] Memory/, direct_manager_role: 'Backend Engineering Manager, Memory & Database') %>
<table>
<thead>
<tr>
<th>Person</th>
<th>Role</th>
</tr>
</thead>
<tbody>
<tr>
<td>Joshua Lambert</td>
<td><a href="/job-families/product/group-manager-product">Group Manager, Product Management, Enablement</a></td>
</tr>
</tbody>
</table>
## Meetings
Where we can we follow the GitLab values and communicate asynchronously.  However, there have a few important recurring meetings.  Please reach out to the [#g_memory](https://gitlab.slack.com/messages/CGN8BUCKC) Slack channel if you'd like to be invited.
* Weekly Memory Team meeting - Mondays 6:00 AM PDT (11:00 AM UTC)
* Memory Team Open Office Hours 
  * Tuesdays alternating between 6:30 AM PDT (11:30 AM UTC) and 12:00 AM PDT (7:00 AM UTC) to create both APAC and EMEA friendly hours

## Work
We follow the GitLab [engineering workflow](/handbook/engineering/workflow/) guidelines.  To bring an issue to our attention please create an issue in the relevant project, or in the [Memory team project](https://gitlab.com/gitlab-org/memory-team/team-tasks/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).  Add the `~"group::memory"` label along with any other relevant labels.  If it is an urgent issue, please reach out to the Product Manager or Engineering Manager listed in the [Stable Counterparts](/handbook/engineering/development/enablement/memory/#stable-counterparts) section above.

### Planning
The Memory Team uses the [Memory by Milestone](https://gitlab.com/groups/gitlab-org/-/boards/1143987?label_name[]=group%3A%3Amemory) board to plan issues for each milestone.  

We are adopting a lightweight weighting system, similar to those adopted by other teams.
* [Plan:Project Management BE Team Capacity Planning](/handbook/engineering/development/dev/plan-project-management-be/#capacity-planning)
* [Create: Source Code BE Team Weights](/handbook/engineering/development/dev/create-source-code-be/#weights)
* [Geo Team Weights](/handbook/engineering/development/enablement/geo/#weights)

#### Say/Do Ratio
We use the `~Deliverable` label to track our Say/Do ratio.  At the beginning of each milestone, during a Memory Team Weekly meeting, we review the issues and determine those issues we are confident we can deliver within the milestone.  The issue will be marked with the `~Deliverable` label.  At the end of the milestone the successfully completed issues with the `~Deliverable` label are tracked in two places.  We have a dashboard in Sisense that will calculate how many were delivered within the mileston and account for issues that were moved.  Additionally, our milestone retro issue lists all of the `~Deliverable` issues shipped along with those that missed the milesone.

### Roadmap
The Memory Team [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Amemory&label_name[]=Roadmap) gives a view of what is currently in flight as well as projects that have been prioritized for the next 3+ months.

### Boards

 * [Memory by Milestone](https://gitlab.com/groups/gitlab-org/-/boards/1143987?label_name[]=group%3A%3Amemory) 
   * The main board used for planning and prioritization.  Issues are placed into milestones and in priority order from top to bottom.  When looking for work, team members should choose the topmost item from this list that is unassigned.
 * [Memory:Workflow Kanban](https://gitlab.com/groups/gitlab-org/-/boards/1065668?&label_name[]=group%3A%3Amemory) 
   * Reflects the current state of the issue (Open, Ready for Development, In Dev, In Review, Blocked, Closed).
 * [Memory:Planning](https://gitlab.com/groups/gitlab-org/-/boards/1077383?&label_name[]=group%3A%3Amemory)
   * A view of issues grouped by Priority or Severity.
 * [Memory by Team Member](https://gitlab.com/groups/gitlab-org/-/boards/1156292?&label_name[]=group%3A%3Amemory)
   * A view of issues grouped by assignee. 


## Links

### Knowledge sharing and lessons learned

 * [Memory Team Approach](./approach.html)
 * [Memory Team Knowledge Sharing](./knowledge.html)
 * Blog post: [Scaling down: How we shrank image transfers by 93%](https://about.gitlab.com/blog/2020/09/21/scaling-down-how-we-prototyped-an-image-scaler-at-gitlab/)

### More about how we work
 * [Memory Team YouTube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq_5ZWIHYfbcAYjtXYcEZA3)
 * [Memory Team subgroup](https://gitlab.com/gitlab-org/memory-team)
 * [Retrospective page](https://gitlab.com/gl-retrospectives/memory-team)
 * Slack Channel [#g_memory](https://gitlab.slack.com/messages/CGN8BUCKC)
 * [Talent skills](/job-families/engineering/backend-engineer/#memory) that help the team
 * [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline)

