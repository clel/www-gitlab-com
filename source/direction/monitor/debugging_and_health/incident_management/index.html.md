---
layout: markdown_page
title: "Category Direction - Incident Management"
description: "A robust incident management platform consumes inputs from all sources and empowers the response team to quickly understand and remediate the problem at hand."
canonical_path: "/direction/monitor/debugging_and_health/incident_management/"
---

- TOC
{:toc}

## Introduction and how you can help
Thanks for visiting this category page on Incident Management in GitLab. This page belongs to the Health group of the Monitor stage, and is maintained by [Sarah Waldner](https://gitlab.com/sarahwaldner) who can be contacted directly via [email](mailto:swaldner@gitlab.com). This vision is a work in progress and everyone can contribute. Sharing your feedback directly on issues and epics at GitLab.com is the best way to contribute to our vision. If you’re a GitLab user and have direct knowledge of your need for incident management, we’d especially love to hear from you.

## Overview

Downtime costs companies an average of $5,600/minute, [according to Gartner](https://blogs.gartner.com/andrew-lerner/2014/07/16/the-cost-of-downtime/).
This number, though an estimate based on a wide range of companies, communicates that downtime is expensive for organizations.
This is especially true for those who have not invested in culminating process and culture around managing these outages and resolving them quickly.
The larger an organization becomes, the more distributed their systems and teams tend to be.
This distribution leads to longer response times and more money lost for the business.
Investing in the right tools and fostering a culture of autonomy, feedback, quality, and automation leads to more time spent innovating and building software and less time spent reacting to outages and racing to restore services.
The tools your DevOps teams use to respond during incidents critically affect [MTTR (Mean Time To Resolve, also known Mean Time To Repair)](https://en.wikipedia.org/wiki/Mean_time_to_repair) as well as the happiness and morale of team members responsible for the IT services your business depends on.

A robust incident management platform consumes inputs from all sources, transforms those inputs into actionable incidents, routes them to the responsible party, and then empowers the response team to quickly understand and remediate the problem at hand. Moreover, this platform should also guide Post Incident Reviews following the fire-fight that makes it easy for the team create and feed after-action items back into the Plan stage for continuous improvement.

### Mission
Our mission is to help DevOps teams reduce MTTR by streamlining the [triage](https://about.gitlab.com/direction/monitor/workflows/triage/) and [resolve](https://about.gitlab.com/direction/monitor/workflows/resolve/) workflows via tools that provide access to observability resources (metrics, logs, errors, runbooks, and traces), that foster easy collaboration across response teams, and that support continuous improvement via Post Incident Reviews and system recommendations.

### Challenges
As we invest R&D in building out Incident Management at GitLab, we are faced with the following challenges:
* The market is dominated by Incident Management companies that have been around for longer. Specific examples include:
   * [ServiceNOW](https://www.servicenow.com/) - founded in 2003
   * [PagerDuty](https://www.pagerduty.com/) - founded in 2009
   * [Splunk VictorOps](https://victorops.com/) - founded in 2012
   * [Atlassian Opsgenie](https://www.opsgenie.com/) - founded in 2012
* We lack brand identification with Enterprise Ops buyers (also mentioned on the [Ops Vision page](https://about.gitlab.com/direction/ops/#challenges))

### Opportunities
We are uniquely positioned to take advantage of the following opportunities:
* Colocation of code and incidents significantly reduces context switching and accelerates [MTTR](https://en.wikipedia.org/wiki/Mean_time_to_repair). We are easily able to correlate development events such as merge requests and deploys with incidents, shortening the time it takes to find the root cause and automates some of the work required to prepare a timeline of events necessary for Post Incident Reviews
* We are well-practiced in building [boring solutions](/handbook/values/#boring-solutions) and [iteration](/handbook/values/#iteration). This will enable us to quickly produce a simple version of Incident Management "just-good-enough" to displace overly complicated existing solutions, while rapidly iterating over the long term towards a lovable product in this category.
* We can dominate the incident response market for [cloud-native applications](/handbook/product/application-types/#cloud-native-web-applications) where incumbent players (like ServiceNow) have been slow to meet user requirements for an integrated, robust understanding of the health of a complex micro-services based application
* We can uniquely serve the needs of Operations Managers who struggle to answer the question - "Are my teams spending all their time firefighting, or are they proactively managing the health of their applications?"
* We can repurpose many existing features within GitLab when we design workflows for Incident management. This will enable us to achieve:
   * Accelerated time to market
   * Quick iterations
   * Faster feature adoption we are building on known workflows and concepts
   * Improvements to existing features will benefit a wider set of use cases beyond Incident Management

## Target Audience and Experience
Our current Incident Management tools have been built for users who align with our [Allison (Application Ops)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#allison-application-ops) and [Devon (DevOps Engineer)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer) personas. The experience targets DevOps teams at smaller companies where it is common for the engineers to be on-call and responding to alerts for the software that they also write code for. As we mature this category, we will evolve the experience to appeal to and serve the enterprise customer.

## Strategy
### Scope
Incident Management is a broad category. The following diagram explains all functionality that is currently within scope for our vision of the category.

![image.png](./incident_management.png)

### Maturity Plan
We are currently working to mature the **Incident Management** category from `viable` to `complete`. Definitions of these maturity levels can be found on [GitLab's Maturity page](https://about.gitlab.com/direction/maturity/). The following epics group the functionality we have planned to mature Incident Management.
* [Complete](https://gitlab.com/groups/gitlab-org/-/epics/1494)
* [Lovable](https://gitlab.com/groups/gitlab-org/-/epics/1539)

### What is Next & Why?
Processing alerts during a fire-fight requires responders to coordinate across multiple tools to evaluate different data sources. This is time consuming because every time a responder switches to a new tool, they are confronted with a new interface and different interactions which is disorienting and slows down investigation, collaboration, and the sharing of findings with teammate. Actionable alerts and incidents accelerate the fire-fight by enabling efficient knowledge sharing, providing guidelines for resolution, and minimizing the number of tools you need to check before finding the problem. In support of this, we are pursuing the following functionality in the next 2-3 releases:

- [Upload screenshots and images of metrics charts directly to incidents](https://gitlab.com/gitlab-org/gitlab/-/issues/235994) to aggregate findings during a fire-fight
- Ability to [create multiple HTTP endpoints with custom mappings](https://gitlab.com/groups/gitlab-org/-/epics/4390) to allow users to integrate any alert source with GitLab
- [On-call Schedule Management](https://gitlab.com/groups/gitlab-org/-/epics/3960) so that response teams can manage schedules and workflows alongside development activities

...and much more! Please follow along in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/3066) to contribute to our plan.

### Dogfooding Plan
We are actively [dogfooding](https://about.gitlab.com/handbook/product/product-processes/#when-do-we-dogfood) Incident Management features with the [Infrastructure team](https://about.gitlab.com/handbook/engineering/infrastructure/team/). Today, the Infrastructure team relies partially on PagerDuty to maintain GitLab.com and the other services they are responsible for. Ultimately the joint goal of the Infrastructure team and the Monitor:Health group is for the Infrastructure team to be able to rely on GitLab Incident Management solely. Our plan to achieve this goal is as follows:

1. Prioritize new functionality based on the [gap analysis](https://gitlab.com/gitlab-org/monitor/health/-/issues/36)
2. Incrementally dogfood new features via simulation days ([example](https://gitlab.com/gitlab-org/gitlab/-/issues/231518)) to gather immediate feedback on for improvements
3. Begin a full migration once we have completed filling in the gap analysis - view migration plan [here](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/322)

## Competitive Landscape
[Atlassian Opsgenie](https://www.opsgenie.com/)
[Splunk VictorOps](https://victorops.com/)  
[PagerDuty](https://www.pagerduty.com/)  
[ServiceNOW](https://www.servicenow.com/products/incident-management.html)  
[XMatters](https://www.xmatters.com/use-cases/major-incident-management-mim/)  

## Analyst Landscape

### 451 Research
451 Research published an article on the acquisition of Rundeck by PagerDuty in September 2020. Read more about this on Rundecks [website](https://www.rundeck.com/pagerduty). This was a strategic move to meet the demands of the enterprise for more automation in incident response.

GitLab has plans to investigate using Rundeck for Runbooks via [gitlab#36655](https://gitlab.com/gitlab-org/gitlab/-/issues/36655), this will be interesting opportunity to connect the PagerDuty lifecycle into GitLab Runbooks and Monitoring capabilities. 

There is an existing landscape of comparable tools and even "ServiceNow and xMatters have orchestration engines that can be deployed to build workflows across tools, but they aren't typically extensively used to execute remediations." VictorOps (owned by Splunk) and OpsGenie (owned by Atlassian) are other similar tools with visions like PagerDuty. 

## Top Customer Success/Sales Issue(s)
Not yet, but accepting merge requests to this document.

## Top Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Internal Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Vision Item(s)
Not yet, but accepting merge requests to this document.
