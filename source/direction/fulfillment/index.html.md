---
layout: markdown_page
title: Product Direction - Fulfillment
description: "The Fulfillment Team at GitLab focus create and support the enablement of our customers to purchase, upgrade, downgrade, and renew licenses and subscriptions."
canonical_path: "/direction/fulfillment/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product direction for GitLab's Fulfillment section. If you'd like to discuss this vision directly with the Product Manager for Fulfillment,
feel free to reach out to [Amanda Rueda](arueda@gitlab.com), [@amandarueda](https://gitlab.com/amandarueda) on GitLab.

Additional pages of interest:

- ##### [Fulfillment internal Release post - check out what we've recently shipped and what's on deck](https://about.gitlab.com/direction/fulfillment/releases/)
- ##### [Purchase group direction](https://about.gitlab.com/direction/fulfillment/purchase/)
- ##### [Provision group direction](https://about.gitlab.com/direction/fulfillment/provision/)

## Overview

The Fulfillment Team at GitLab focuses on creating and supporting the experiences that enable our customers to purchase, upgrade, downgrade, and renew licenses and subscriptions. We are constantly striving to make the billing and payment processes simple, intuitive, and stress-free for our customers and GitLab team members. We believe that engaging in business transactions with GitLab should be minimally invasive and when noticed, should be a positive, empowering, and user-friendly experience. 

We also partner with the [Growth Groups](https://about.gitlab.com/direction/#growth-groups) to enable experimentation and product improvements focused on making it easy for people to try, buy, grow, and stay with GitLab. 


## Themes

* Make it easy and simple as possible to purchase, upgrade, and renew a GitLab license or subscription 
* Build and support a robust billing and licensing experience that is flexible and allows us to continue to iterate on our pricing, packaging, and product and service offerings
* Provide transparent, clear, and easy-to-understand information to admins and group owners about their users, usage, and corresponding costs 
* Automate as much of the billing process as possible so our GitLab team members can focus their time on providing excellent service and support to our customers
* Upholding our compliance, privacy, and security standards
 
## How we prioritize

We follow the same prioritization guidelines as the [product team at large](https://about.gitlab.com/handbook/product/product-processes/#how-we-prioritize-work). Issues tend to flow from having no milestone, to being added to the backlog, to a directional milestone (e.g. Next 3-4 releases), and are finally assigned a specific milestone.

Our entire public backlog for Fulfillment can be viewed [on the issue board](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Afulfillment), and can be filtered by labels or milestones. If you find something you are interested in, you're encouraged to jump into the conversation and participate. At GitLab, everyone can contribute!

## What we're currently focused on 


### Making our billing more transparent

We don't want our customers to be surprised about billing and user management. Customers should be fully aware when they're taking an action that will require additional payment. 

* [Send a monthly user digest to subscription management contacts monthly to monitor seat utilization at a glance.](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/1344)
* [Create an in-app banner to notify admin when the subscribed seat count is approaching the limit.](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/1210)
* [Display an in-app banner to notify admin when the subscribed seat count has exceeded the limit.](https://gitlab.com/gitlab-org/gitlab/-/issues/215959)

### Improving user information for admins and group owners

We want customers to easily understand what (and who) they're paying for. Over the next two quarters we will provide instance admins and group owners with tools to find the users they are being billed for.



### Improving the user experience of our CustomersDot

The CustomersDot should be easy-to-use and provide a consistently delightful experience that matches our other offerings.

[The CustomersDot should use our Pajamas design system](https://gitlab.com/groups/gitlab-org/-/epics/1788) 



## Where we're headed 

We are in the early phases of moving towards a simpler approach for how self-managed instances purchase and apply a license. We recognize that the current process is less than ideal for our customers, as well as our GitLab team members. The requirement to log into the CustomersDot to purchase, renew, and access your license key can be frustrating and difficult and we want to make that process an afterthought. 

What we would like to do is create a license sync that will allow customers to regularly sync their license and user counts with GitLab to streamline purchases and renewals as well as enable us to provide more flexible billing options for our customers. 

At the same time, we understand that not all of our customers are running instances that can connect to the internet or have requirements against sharing any kind of information. In those cases, we need to ensure customers have the option to continue to purchase and apply licenses as they do today. 

We are just beginning the technical discovery phase of this effort and we hope to have an initial iteration ready by mid-2020. If you would like to discuss this, or anything else Fulfillment related, please reach out to Amanda Rueda via [e-mail](mailto:arueda@gitlab.com).
