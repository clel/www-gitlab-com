### Tech Stack - New system

Please don't merge this update before the Business Systems Analysts have reviewed and approved. Please make sure to complete the privacy review issue as soon as possible as we won't be able to merge until that is completed. 

Please answer the questions below:

1. Has a [Privacy Review been completed?](https://about.gitlab.com/handbook/engineering/security/dpia-policy/#data-protection-impact-assessment-policy): 
    * [ ] Yes; **Link completed DPIA Issue in the MR comments**
    * [ ] No; **Complete [Privacy Review issue](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/new?issuable_template=Vendor-Procurement-Privacy-Review)**

3. We deprovision access to all systems in our tech stack when a person leaves GitLab. Can you please indicate whether:
    - [ ] All GitLab team members need to be offboarded from this system
    - [ ] Only certain team members need to be offboarded from this system (if the team members are scattered across too many departments or the system you are responsible for contains red data, please go with option 1)
        - If you choose this option, please list all the departments that team members who need to be offboarded belong to: {+List departments here+}  

4. Does this system need to be added to the list of applications in the [GDPR Information Request](https://gitlab.com/gitlab-com/gdpr-request/-/blob/master/.gitlab/issue_templates/gdpr_section_15_request.md) issue template?
   * [ ]  If yes, please create an MR and add the system to the template. Once completed, please paste the link in the comments of this MR.
5. Does this system need to be added to the applications in the [GDPR deletion request issue template](https://gitlab.com/gitlab-com/gdpr-request/-/blob/master/.gitlab/issue_templates/deletion_meta_issue.md)?
   * [ ]  If yes, please create an MR and add the system to the template. Once completed, please paste the link in the comments of this MR.

6. Please [create an issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Update_Tech_Stack_Provisioner) to add the provisioners of the tool to the provisioners group. Link the issue to the comments of this MR.

1. Does data from this system need to be integrated into the [Enterprise Data Warehouse](https://about.gitlab.com/handbook/business-ops/data-team/#-data-warehouse) for reporting and analytics? Please answer with `Yes` or `No`. {+Add your answer here+}
    - If answer is yes: Create a '[New Data Source](https://gitlab.com/gitlab-data/analytics/-/issues/new?issuable_template=New%20Data%20Source)' issue in the Data Project. Keep in mind that new data sources are not free and the cost should be included in the total cost of the project. The cost of an automated data pipeline connector is arond usually 5-10k depending on complexity. If there is no budget OR no existing connector, the data will require custom development. For custom development, please create an issue and discuss possible options with the data team.  

> Examples for why you would need data integrated into the EDW:
  > - the data will be used as part of a new Key Performance Indicator or Performance Indicator
  > - the data needs to be part of lead-to-cash analysis
  > - the data needs to be joined with Marketo, Salesforce, or NetSuite data for cross-system analysis



### To dos before merging (@lisvinueza)

- [ ] Ensure privacy review has been linked
- [ ] Ensure all questions above have been answered and all action items have been completed
- [ ] Create an MR to update the relevant [offboarding tasks](https://gitlab.com/gitlab-com/people-group/employment-templates/-/tree/master/.gitlab/issue_templates) based on the answers to question 3. 




/cc @SChia @pranavpolisetty @sheetaljain @lisvinueza @kxkue
/assign @lisvinueza
/label ~"BusinessTechnology" ~"BT - TechStack" ~"BT-TechStack::To do" 
